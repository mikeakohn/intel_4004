// Intel 4004 FPGA Soft Processor
//  Author: Michael Kohn
//   Email: mike@mikekohn.net
//     Web: https://www.mikekohn.net/
//   Board: iceFUN iCE40 HX8K
// License: MIT
//
// Copyright 2023 by Michael Kohn

module i4004
(
  output [7:0] leds,
  output [3:0] column,
  input raw_clk,
  output eeprom_cs,
  output eeprom_clk,
  output eeprom_di,
  input  eeprom_do,
  output speaker_p,
  output speaker_m,
  output ioport_0,
  input  button_reset,
  input  button_halt,
  input  button_program_select,
  input  button_0
);

// iceFUN 8x4 LEDs used for debugging.
reg [7:0] leds_value;
reg [3:0] column_value;

assign leds = leds_value;
assign column = column_value;

// Memory bus (ROM, RAM, peripherals).
reg [15:0] mem_address = 0;
reg [7:0] mem_write = 0;
reg [3:0] mem_write_mask = 0;
wire [7:0] mem_read;
//wire mem_data_ready;
reg mem_bus_enable = 0;
reg mem_write_enable = 0;

// Clock.
reg [21:0] count = 0;
reg [5:0]  state = 0;
reg [4:0] clock_div;
reg [14:0] delay_loop;
wire clk;
assign clk = clock_div[1];

// Registers.
reg [15:0] pc = 0;
reg [15:0] ea;
reg [3:0] accum = 0;
reg c_flag;
reg [7:0] index [15:0];
reg [4:0] temp;
reg [2:0] data_bank;

reg test_signal = 1;

// Instruction.
reg [7:0] instruction;
wire [3:0] reg_num;
wire [2:0] reg_pair;
assign reg_num = instruction[3:0];
assign reg_pair = instruction[3:1];

// Stack.
reg [12:0] stack [2:0];
reg [1:0] sp;

// Eeprom.
reg  [8:0] eeprom_count;
wire [7:0] eeprom_data_out;
reg [10:0] eeprom_address;
reg eeprom_strobe = 0;
wire eeprom_ready;

// Debug.
//reg [7:0] debug_0 = 0;
//reg [7:0] debug_1 = 0;
//reg [7:0] debug_2 = 0;
//reg [7:0] debug_3 = 0;

// This block is simply a clock divider for the raw_clk.
always @(posedge raw_clk) begin
  count <= count + 1;
  clock_div <= clock_div + 1;
end

// Debug: This block simply drives the 8x4 LEDs.
always @(posedge raw_clk) begin
  case (count[9:7])
    3'b000: begin column_value <= 4'b0111; leds_value <= { 3'b111, ~c_flag, ~accum[3:0] }; end
    3'b010: begin column_value <= 4'b1011; leds_value <= ~instruction[7:0]; end
    3'b100: begin column_value <= 4'b1101; leds_value <= ~pc[7:0]; end
    3'b110: begin column_value <= 4'b1110; leds_value <= ~state; end
    default: begin column_value <= 4'b1111; leds_value <= 8'hff; end
  endcase
end

parameter STATE_RESET =                 0;
parameter STATE_DELAY_LOOP =            1;
parameter STATE_FETCH_OP_0 =            2;
parameter STATE_FETCH_OP_1 =            3;
parameter STATE_START_DECODE =          4;

parameter STATE_WRITE_BACK_ACCUM =      5;
parameter STATE_FETCH_IMMEDIATE_0 =     6;
parameter STATE_FETCH_IMMEDIATE_1 =     7;
parameter STATE_FETCH_INDIRECT_0 =     11;
parameter STATE_FETCH_INDIRECT_1 =     12;
//parameter STATE_INC_AND_SKIP_0 =       13;
//parameter STATE_INC_AND_SKIP_1 =       14;

parameter STATE_FETCH_DATA_TO_PAIR_0 = 14;
parameter STATE_FETCH_DATA_TO_PAIR_1 = 15;

parameter STATE_HALTED =       40;
parameter STATE_ERROR =        41;
parameter STATE_EEPROM_START = 42;
parameter STATE_EEPROM_READ =  43;
parameter STATE_EEPROM_WAIT =  44;
parameter STATE_EEPROM_WRITE = 45;
parameter STATE_EEPROM_DONE =  46;

parameter STATE_DEBUG =        47;
parameter STATE_MEM_DEBUG_0 =  48;
parameter STATE_MEM_DEBUG_1 =  49;
parameter STATE_MEM_DEBUG_2 =  50;
parameter STATE_MEM_DEBUG_3 =  51;

// This block is the main CPU instruction execute state machine.
always @(posedge clk) begin
  if (!button_reset)
    state <= STATE_RESET;
  else if (!button_halt)
    state <= STATE_HALTED;
  else
    case (state)
      STATE_RESET:
        begin
          mem_address <= 0;
          mem_write_enable <= 0;
          mem_write <= 0;
          instruction <= 0;
          delay_loop <= 12000;
          //eeprom_strobe <= 0;
          state <= STATE_DELAY_LOOP;
          accum <= 0;
          c_flag <= 0;
          sp <= 0;
        end
      STATE_DELAY_LOOP:
        begin
          // This is probably not needed. The chip starts up fine without it.
          if (delay_loop == 0) begin

            // If button is not pushed, start rom.v code otherwise use EEPROM.
            if (button_program_select) begin
              pc <= 16'h4000;
            end else begin
              pc <= 0;
            end

            //state <= STATE_EEPROM_START;
            state <= STATE_FETCH_OP_0;
          end else begin
            delay_loop <= delay_loop - 1;
          end
        end
      STATE_FETCH_OP_0:
        begin
          mem_bus_enable <= 1;
          mem_write_enable <= 0;
          mem_address <= pc;
          pc <= pc + 1;
          state <= STATE_FETCH_OP_1;
        end
      STATE_FETCH_OP_1:
        begin
          mem_bus_enable <= 0;
          instruction <= mem_read;
          state <= STATE_START_DECODE;
        end
      STATE_START_DECODE:
        begin
          case (instruction[7:4])
            4'b0000:
              // nop (no operation).
              state <= STATE_FETCH_OP_0;
            4'b0001:
              // jcn (jump conditional).
              state <= STATE_FETCH_IMMEDIATE_0;
            4'b0010:
              begin
                if (instruction[0] == 0) begin
                  // fim (fetch immediate).
                  ea <= { pc[9:8], index[0], index[1] };
                  state <= STATE_FETCH_DATA_TO_PAIR_0;
                end else begin
                  // src (send register control).
                  state <= STATE_ERROR;
                end
              end
            4'b0011:
              begin
                if (instruction[0] == 0) begin
                  // fin (fetch indirect).
                  state <= STATE_FETCH_INDIRECT_0;
                end else begin
                  // jin (jump indirect).
                  pc[7:4] <= index[{ reg_pair, 0 }];
                  pc[3:0] <= index[{ reg_pair, 1 }];
                  state <= STATE_FETCH_OP_0;
                end
              end
            4'b0100:
              begin
                // jun (jump unconditional).
                state <= STATE_FETCH_IMMEDIATE_0;
              end
            4'b0101:
              begin
                // jms (jump subroutine).
                state <= STATE_FETCH_IMMEDIATE_0;
              end
            4'b0110:
              begin
                // inc (increment index register).
                index[reg_num] <= index[reg_num] + 1;
                state <= STATE_FETCH_OP_0;
              end
            4'b0110:
              begin
                // isz (increment index register and skip).
                index[reg_num] <= index[reg_num] + 1;
                state <= STATE_FETCH_IMMEDIATE_0;
              end
            4'b1000:
              begin
                // add (add index register to accumulator with carry).
                temp <= index[reg_num] + accum + c_flag;
                state <= STATE_WRITE_BACK_ACCUM;
              end
            4'b1001:
              begin
                // sub (sub index register to accumulator with carry).
                temp <= accum + ~index[reg_num] + c_flag;
                state <= STATE_WRITE_BACK_ACCUM;
              end
            4'b1010:
              begin
                // ld (load accumulator with contents of index register).
                accum <= index[reg_num];
                state <= STATE_FETCH_OP_0;
              end
            4'b1011:
              begin
                // xch (exchange index register and accumulator).
                accum <= index[reg_num];
                index[reg_num] <= accum;
                state <= STATE_FETCH_OP_0;
              end
            4'b1100:
              begin
                // bbl (brach back and load).
                accum <= instruction[3:0];
                pc <= stack[sp - 1];
                sp <= sp - 1;
                state <= STATE_FETCH_OP_0;
              end
            4'b1101:
              begin
                // ldm (load immediate).
                accum <= instruction[3:0];
                state <= STATE_FETCH_OP_0;
              end
            4'b1110:
              begin
                // Some memory instructions?
                state <= STATE_ERROR;
              end
            4'b1111:
              begin
                case (instruction[3:0])
                  // 0000: clb (clear both).
                  // 0001: clc (clear carry).
                  // 0010: iac (increment accumulator).
                  // 0011: cmc (complement carry).
                  // 0100: cma (complement accumulator).
                  // 0101: ral (rotate left).
                  // 0110: rar (rotate right).
                  // 0111: tcc (transfer carry and clear).
                  // 1000: dac (decrement accumulator).
                  // 1001: tcs (transfer carry subtract).
                  // 1010: stc (set carry).
                  // 1011: daa (decimal adjust accumulator).
                  // 1100: kbp (keyboard process).
                  // 1101: dcl (designate command line).
                  4'b0000: begin accum <= 0; c_flag <=0; end
                  4'b0001: c_flag <= 0;
                  4'b0010: accum <= accum + 1;
                  4'b0011: c_flag <= ~c_flag;
                  4'b0100: accum <= ~accum;
                  4'b0101:
                    begin
                      c_flag <= accum[3];
                      accum <= { accum[2:0], c_flag };
                    end
                  4'b0110:
                    begin
                      c_flag <= accum[0];
                      accum <= { c_flag, accum[3:1] };
                    end
                  4'b0111: begin accum <= c_flag; c_flag <= 0; end
                  4'b1000: accum <= accum - 1;
                  4'b1001:
                    begin
                      if (c_flag == 0)
                        accum <= 4'b1001;
                      else
                        accum <= 4'b1010;

                      c_flag <= 0;
                    end
                  4'b1010: c_flag <= 1;
                  4'b1011:
                    begin
                      if (c_flag || accum > 9) begin
                        accum <= accum + 6;
                        if (accum > 9) c_flag <= 1;
                      end
                    end
                  4'b1100:
                    begin
                      case (accum)
                        4'b0000: accum <= 4'b0000;
                        4'b0001: accum <= 4'b0001;
                        4'b0010: accum <= 4'b0010;
                        4'b0100: accum <= 4'b0011;
                        4'b1000: accum <= 4'b0100;
                        default: accum <= 4'b1111;
                      endcase
                    end
                  4'b1101: data_bank <= accum[2:0];
                endcase

                state <= STATE_FETCH_OP_0;
              end
          endcase
        end
      STATE_WRITE_BACK_ACCUM:
        begin
          accum <= temp[3:0];
          c_flag <= temp[4];
          state <= STATE_FETCH_OP_0;
        end
      STATE_FETCH_IMMEDIATE_0:
        begin
          mem_bus_enable <= 1;
          mem_write_enable <= 0;
          mem_address <= pc;
          pc <= pc + 1;
          state <= STATE_FETCH_IMMEDIATE_1;
        end
      STATE_FETCH_IMMEDIATE_1:
        begin
          mem_bus_enable <= 0;

          case (instruction[7:4])
            4'b0001:
              begin
                // jcn
                if (instruction[3] == 0) begin
                  if ((instruction[2] && accum == 0) ||
                      (instruction[1] && c_flag == 1) ||
                      (instruction[0] && test_signal == 0))
                  begin
                    pc[7:0] <= mem_read[7:0];
                  end
                end else begin
                  if ((instruction[2] && accum != 0) ||
                      (instruction[1] && c_flag == 0) ||
                      (instruction[0] && test_signal == 1))
                  begin
                    pc[7:0] <= mem_read[7:0];
                  end
                end
              end
            4'b0010:
              begin
                // fim
                index[{ reg_pair, 0 }] <= mem_read[7:4];
                index[{ reg_pair, 1 }] <= mem_read[3:0];
              end
            4'b0100:
              begin
                // jun
                pc <= { instruction[3:0], mem_read[7:0] };
              end
            4'b0100:
              begin
                // jms
                stack[sp] <= pc;
                sp <= sp + 1;
                pc <= { instruction[3:0], mem_read[7:0] };
              end
            4'b0111:
              begin
                // isz
                if (index[reg_num] == 0) begin
                  pc[7:0] <= mem_read[7:0];
                end
              end
          endcase

          state <= STATE_FETCH_OP_0;
        end
      STATE_FETCH_INDIRECT_0:
        begin
          // fin
          mem_bus_enable <= 1;
          mem_write_enable <= 0;
          mem_address <= { pc[15:8], index[0], index[1] };
          state <= STATE_FETCH_INDIRECT_1;
        end
      STATE_FETCH_INDIRECT_1:
        begin
          mem_bus_enable <= 0;
          index[{ reg_pair, 0 }] <= mem_read[7:4];
          index[{ reg_pair, 1 }] <= mem_read[3:0];
          state <= STATE_FETCH_OP_0;
        end
      STATE_FETCH_DATA_TO_PAIR_0:
        begin
          mem_bus_enable <= 1;
          mem_write_enable <= 0;
          mem_address <= ea;
          state <= STATE_FETCH_DATA_TO_PAIR_1;
        end
      STATE_FETCH_DATA_TO_PAIR_1:
        begin
          mem_bus_enable <= 0;
          index[{ reg_pair, 0 }] <= mem_read[7:4];
          index[{ reg_pair, 1 }] <= mem_read[3:0];
          state <= STATE_FETCH_OP_0;
        end
/*
      STATE_INC_AND_SKIP_0:
      STATE_INC_AND_SKIP_1:
*/
/*
      STATE_CAL_PUSH_CR_0:
        begin
          mem_bus_enable <= 1;
          mem_write_enable <= 1;
          mem_address <= lsp + 2;
          mem_write <= cr;
          state <= STATE_CAL_PUSH_CR_1;
        end
      STATE_CAL_PUSH_CR_1:
        begin
          mem_bus_enable <= 0;
          mem_write_enable <= 0;
          mem_write <= 0;
          state <= STATE_CAL_WRITE_LSP_0;
        end
*/
      STATE_HALTED:
        begin
          state <= STATE_HALTED;
        end
      STATE_ERROR:
        begin
          state <= STATE_ERROR;
        end
      STATE_EEPROM_START:
        begin
          // Initialize values for reading from SPI-like EEPROM.
          if (eeprom_ready) begin
            eeprom_count <= 0;
            state <= STATE_EEPROM_READ;
          end
        end
      STATE_EEPROM_READ:
        begin
          // Set the next EEPROM address to read from and strobe.
          eeprom_address <= eeprom_count;
          mem_bus_enable <= 1;
          mem_address <= eeprom_count;
          eeprom_strobe <= 1;
          state <= STATE_EEPROM_WAIT;
        end
      STATE_EEPROM_WAIT:
        begin
          // Wait until 8 bits are clocked in.
          eeprom_strobe <= 0;

          if (eeprom_ready) begin
            mem_bus_enable <= 0;
            mem_write <= eeprom_data_out;
            eeprom_count <= eeprom_count + 1;
            state <= STATE_EEPROM_WRITE;
          end
        end
      STATE_EEPROM_WRITE:
        begin
          // Write value read from EEPROM into memory.
          mem_bus_enable <= 1;
          mem_write_enable <= 1;
          state <= STATE_EEPROM_DONE;
        end
      STATE_EEPROM_DONE:
        begin
          // Finish writing and read next byte if needed.
          mem_bus_enable <= 0;
          mem_write_enable <= 0;

          if (eeprom_count == 256)
            state <= STATE_FETCH_OP_0;
          else
            state <= STATE_EEPROM_READ;
        end
      STATE_DEBUG:
        begin
          state <= STATE_DEBUG;
        end
      STATE_MEM_DEBUG_0:
        begin
          mem_bus_enable <= 1;
          mem_write_enable <= 1;
          mem_address <= 10;
          mem_write <= 8'h55;
          state <= STATE_MEM_DEBUG_1;
        end
      STATE_MEM_DEBUG_1:
        begin
          mem_bus_enable <= 0;
          mem_write_enable <= 0;
          mem_write <= 0;
          state <= STATE_MEM_DEBUG_2;
        end
      STATE_MEM_DEBUG_2:
        begin
          mem_bus_enable <= 1;
          mem_write_enable <= 0;
          mem_address <= 14'h2009;
          //mem_address <= ea;
          state <= STATE_MEM_DEBUG_3;
        end
      STATE_MEM_DEBUG_3:
        begin
          mem_bus_enable <= 0;
          accum <= mem_read;
          state <= STATE_DEBUG;
        end
    endcase
end

memory_bus memory_bus_0(
  .address      (mem_address),
  .data_in      (mem_write),
  //.write_mask   (mem_write_mask),
  .data_out     (mem_read),
  //.data_ready   (mem_data_ready),
  .bus_enable   (mem_bus_enable),
  .write_enable (mem_write_enable),
  .clk          (clk),
  .raw_clk      (raw_clk),
   //.double_clk  (clock_div[1]),
  .speaker_p    (speaker_p),
  .speaker_m    (speaker_m),
  .ioport_0     (ioport_0),
  .button_0     (button_0),
  .reset        (~button_reset)
  //.debug        (debug_1)
);

eeprom eeprom_0
(
  .address    (eeprom_address),
  .strobe     (eeprom_strobe),
  .raw_clk    (raw_clk),
  .eeprom_cs  (eeprom_cs),
  .eeprom_clk (eeprom_clk),
  .eeprom_di  (eeprom_di),
  .eeprom_do  (eeprom_do),
  .ready      (eeprom_ready),
  .data_out   (eeprom_data_out)
);

endmodule

