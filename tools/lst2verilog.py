#!/usr/bin/env python3

import sys

print("// Intel 4004 FPGA Soft Processor")
print("//  Author: Michael Kohn")
print("//   Email: mike@mikekohn.net")
print("//     Web: https://www.mikekohn.net/")
print("//   Board: iceFUN iCE40 HX8K")
print("// License: MIT")
print("//")
print("// Copyright 2023 by Michael Kohn\n\n")

print("module rom")
print("(")
print("  input  [9:0] address,")
print("  output [7:0] data_out")
print(");\n")

print("reg [7:0] data;")
print("assign data_out = data;\n")

print("always @(address) begin")
print("  case (address)")

indent = "    "
address = 0
org = 0

fp = open(sys.argv[1], "r")

for line in fp:
  if line.startswith(".org"):
    line = line.replace(".org", "").strip()
    address = int(line, 0)
    org = address
    continue

  if not line.startswith("0x"): continue
  line = line.strip()

  tokens = line.split(":")

  a = tokens[0]
  opcode = tokens[1]

  instruction = opcode[8:20].strip()
  opcodes = opcode[0:8].strip().split()

  #print(a)
  #print(instruction)
  #print(opcodes)

  a = int(a, 16)

  if address != a:
    print("Error: Address " + str(address) + " does not equal " + str(a))
    sys.exit(1)

  if instruction != "":
    print(indent + "// " + instruction)

  for opcode in opcodes:
    print(indent + str(address - org) + ": data <= 8'h" + opcode + ";")
    address += 1

fp.close()

print("    default: data <= 0;")
print("  endcase")
print("end\n")

print("endmodule\n")

